<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $req) {
        // dd($req->all()); // Data debugging
        $nama = $req['first-name'] . ' '. $req['last-name'];

        return view('welcome', ['nama' => $nama]);
    }
}
