<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index() {
      $casts = DB::table('cast')->get();
      return view("pages.casts", compact("casts"));
    }

    public function create() {
      return view("pages.cast.form");
    }

    public function store(Request $req) {
      $req->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required'
      ]);

      DB::table('cast')->insert([
        'nama' => $req['nama'],
        'umur' => $req['umur'],
        'bio' => $req['bio']
      ]);

      return redirect('/cast');
    }

    public function show($cast_id) {
      $cast = DB::table('cast')->where('id', $cast_id)->first();
      return view('pages.cast.show', compact('cast'));
    }

    public function edit($cast_id) {
      $cast = DB::table('cast')->where('id', $cast_id)->first();
      return view('pages.cast.edit', compact('cast'));
    }

    public function update($cast_id, Request $req) {
      $req->validate([
        'nama' => 'required',
        'umur' => 'required',
        'bio' => 'required'
      ]);

      DB::table('cast')->where('id', $cast_id)->update([
        'nama' => $req['nama'],
        'umur' => $req['umur'],
        'bio' => $req['bio']
      ]);

      return redirect('/cast');
    }

    public function destroy($cast_id) {
      DB::table('cast')->where('id', $cast_id)->delete();
      return redirect('/cast');
    }    
}
