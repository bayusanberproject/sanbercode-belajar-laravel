<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TableController extends Controller
{
    public function table() {
        return view('pages.table');
    }

    public function datatable() {
        return view('pages.datatable');
    }
}
