<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profile', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('user');
        });

        Schema::table('kritik', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('user');
            $table->foreign('film_id')->references('id')->on('film');
        });

        Schema::table('film', function (Blueprint $table) {
            $table->foreign('genre_id')->references('id')->on('genre');
        });

        Schema::table('peran', function (Blueprint $table) {
            $table->foreign('cast_id')->references('id')->on('cast');
            $table->foreign('film_id')->references('id')->on('film');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profile', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::table('kritik', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['film_id']);
        });

        Schema::table('film', function (Blueprint $table) {
            $table->dropForeign(['genre_id']);
        });

        Schema::table('peran', function (Blueprint $table) {
            $table->dropForeign(['cast_id']);
            $table->dropForeign(['film_id']);
        });
    }
}
