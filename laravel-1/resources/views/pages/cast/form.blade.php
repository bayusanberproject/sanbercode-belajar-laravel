@extends('adminlte.master')

@push('css')
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@section('content.header')
  <h1>Tambah Cast</h1>
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Tambah Cast</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <form action="/cast" method="POST">
      @csrf
      <div class="form-group">
        <label for="exampleInputEmail1">Nama</label>
        <input type="text" required class="form-control" id="name" name="nama">
        @error('nama')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Umur</label>
        <input type="number" required class="form-control" id="age" name="umur">
        @error('umur')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <div class="form-group">
        <label for="exampleInputEmail1">Bio</label>
        <textarea required type="number" required class="form-control" id="bio" name="bio"></textarea>
        @error('bio')
          <div class="invalid-feedback">
            {{ $message }}
          </div>
        @enderror
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
  <!-- /.card-body -->
</div>
@endsection