@extends('adminlte.master')

@push('css')
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@section('content.header')
  <h1>Cast #{{ $cast->id }}</h1>
@endsection

@section('content')
<div class="card">
      <div class="card-header">
        <h3 class="card-title">Detail Cast</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <p>ID Cast : {{ $cast->id }}</p>
        <p>Nama : {{ $cast->nama }}</p>
        <p>Umur : {{ $cast->umur }}</p>
        <p>Bio : {{ $cast->bio }}</p>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <a type="button" class="btn btn-info" href="/cast/{{ $cast->id }}/edit">Edit</a>
        <form action="/cast/{{ $cast->id }}" method="POST" style="display: inline-block">
          @csrf
          @method('DELETE')
          <button type="submit" class="btn btn-danger">Delete</button>
        </form>
      </div>
    </div>
@endsection

@push('script')
  <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@endpush