@extends('adminlte.master')

@push('css')
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
@endpush

@section('content.header')
  <h1>Daftar Cast</h1>
@endsection

@section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Daftar Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <a href="/cast/create" class="btn btn-primary mb-3">Tambah</a>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse ($casts as $idx=>$cast)
            <tr>
              <td>{{ $idx + 1 }}</td>
              <td>{{ $cast->nama }}</td>
              <td>{{ $cast->umur }}</td>
              <td>{{ $cast->bio }}</td>
              <td>
                <form action="/cast/{{ $cast->id }}" method="POST" style="display: inline-block">
                  @csrf
                  @method('DELETE')
                  <button type="submit" class="btn btn-danger">Delete</button>
                </form>
                <a href="/cast/{{ $cast->id }}" class="btn btn-link">Show Detail</a>
              </td>
            </tr>
          @empty
              <tr>
                <td colspan="5" style="text-align: center; font-weight: bold">No Data</td>
              </tr>
          @endforelse
        </tbody>
        <tfoot>
          <tr>
            <th>ID</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection

@push('script')
  <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('script')
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush