<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function() {
  return view('adminlte/master');
});

Route::get('/table', 'TableController@table');
Route::get('/data-table', 'TableController@datatable');

# Cast Router
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

Route::post("/cast", 'CastController@store');
Route::put("/cast/{cast_id}", 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
