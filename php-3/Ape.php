<?php

require_once "Animal.php";

class Ape extends Animal {
  public  function __construct(string $name) {
    parent::__construct($name);
    $this->legs = 4;
  }

  public function yell() {
    echo "Auooo". "<br/>";
  }

}
