<?php

require_once "Animal.php";

class Frog extends Animal {
  public  function __construct(string $name) {
    parent::__construct($name);
    $this->legs = 4;
  }

  public function jump() {
    echo "hop hop". "<br/>";
  }

}
