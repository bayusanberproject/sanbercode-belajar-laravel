<?php

require_once "Animal.php";
require_once "Ape.php";
require_once "Frog.php";

$sheep = new Animal("shaun");

echo "Nama : ";
echo $sheep->name ."<br/>"; // "shaun"
echo "Legs : ";
echo $sheep->legs ."<br/>"; // 4
echo "cold blooded : ";
echo $sheep->cold_blooded. "<br/>"; // "no"

echo "<br/>";

$kodok = new Frog("buduk");

echo "Nama : ";
echo $kodok->name ."<br/>"; // "shaun"
echo "Legs : ";
echo $kodok->legs ."<br/>"; // 4
echo "cold blooded : ";
echo $kodok->cold_blooded. "<br/>"; // "no"

echo "Jump : ";
$kodok->jump(); // "hop hop"

echo "<br/>";

$sungokong = new Ape("kera sakti");

echo "Nama : ";
echo $sungokong->name ."<br/>"; // "shaun"
echo "Legs : ";
echo $sungokong->legs ."<br/>"; // 4
echo "cold blooded : ";
echo $sungokong->cold_blooded. "<br/>"; // "no"

echo "Yell : ";
$sungokong->yell(); // "Auooo"
