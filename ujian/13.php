<?php
/**
 * Asumsi: Hanya menerima huruf kecil saja
 */
function ubah_huruf($string){
  $result = "";
  $charA = ord('a');
  $charZ = ord('z');
  
  for($i = 0; $i < strlen($string); $i++){
    $result .= chr((ord($string[$i])-$charA + 1) % ($charZ-$charA + 1) + $charA);
  }

  return $result;
}

// TEST CASES
echo ubah_huruf('wow').PHP_EOL; // xpx
echo ubah_huruf('developer').PHP_EOL; // efwfmpqfs
echo ubah_huruf('laravel').PHP_EOL; // mbsbwfm
echo ubah_huruf('keren').PHP_EOL; // lfsfo
echo ubah_huruf('semangat').PHP_EOL; // tfnbohbu
echo ubah_huruf('zebra').PHP_EOL; // tfnbohbu
echo ubah_huruf('kuyang').PHP_EOL; // tfnbohbu

?>