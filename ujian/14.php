<?php
function tukar_besar_kecil($string){
  //kode di sini
  $result = '';

  for($i = 0; $i < strlen($string); $i++){
    if(ctype_lower($string[$i])){
      $result .= strtoupper($string[$i]);
    }else{
      $result .= strtolower($string[$i]);
    }
  }

  return $result;
}

// TEST CASES
echo tukar_besar_kecil('Hello World'). PHP_EOL; // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'). PHP_EOL; // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'). PHP_EOL; // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'). PHP_EOL; // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'). PHP_EOL; // "001-a-3-5tRDyw"

?>