# No. 15
CREATE TABLE customers (
  id INTEGER auto_increment primary key,
  name varchar(255),
  email varchar(255),
  password varchar(255)
);


CREATE TABLE orders (
    id INTEGER auto_increment primary key,
    amount int(8),
    customer_id integer,
    foreign key (customer_id) references customers(id)
);


# No. 16
INSERT INTO customers(name, email, password)
VALUES
    ('John Doe', 'john@doe.com', 'john123'),
    ('Jane Doe', 'jane@doe.com', 'jenita123');

INSERT INTO orders(amount, customer_id)
VALUES
    (500, 1),
    (200, 2),
    (750, 2),
    (250, 1),
    (400, 2);

# No. 17
SELECT name customer_name, SUM(amount) total_amount
FROM orders JOIN customers c on c.id = orders.customer_id
GROUP BY c.id;

# Post Soal
drop table orders;
drop table customers;
